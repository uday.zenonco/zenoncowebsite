import React, { Component } from 'react';
import './App.css';
import Header from './components/homepage/Header';
import Sect1 from './components/homepage/Sect1';
import Sect2 from './components/homepage/Sect2';
import Sect3 from './components/homepage/Sect3';
import Sect4 from './components/homepage/Sect4';
import Sect5 from './components/homepage/Sect5';
import Sect6 from './components/homepage/Sect6';
import Sect7 from './components/homepage/Sect7';
import Sect8 from './components/homepage/Sect8';
import Sect9 from './components/homepage/Sect9';
import Sect10 from './components/homepage/Sect10';
import Sect11 from './components/homepage/Sect11';
import Sect12 from './components/homepage/Sect12';
import Sect13 from './components/homepage/Sect13';
import Sect14 from './components/homepage/Sect14';
import Sect15 from './components/homepage/Sect15';


class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Sect1 />
        <Sect2 />
        <Sect3 />
        <Sect4 />
        <Sect5 />
        <Sect6 />
        <Sect7 />
        <Sect8 />
        <Sect9 />
        <Sect10 />
        <Sect11 />
        <Sect12 />
        <Sect13 />
        <Sect14 />
        <Sect15 />
      </div>
    );
  }
}

export default App;
