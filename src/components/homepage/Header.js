import React, { Component } from 'react';
import logo from "../../images/logo.png";


export default class Header extends Component {
    render() {
        return (
            <div>
                <div className="container">
                <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <img src={logo} className="image-responsive center-block f-left w-300" 
                        style={{width:"200px",paddingTop:"10px",paddingBottom:"10px"}} />
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <p className="h-color f-rubik t-center wd-100" style={{paddingTop:"20px"}}>
                            <span style={{paddingRight:"22px",letterSpacing:"2px"}}><i className="fa fa-phone" aria-hidden="true"></i>&nbsp;+91 937 297 6783</span>
                            <span style={{paddingRight:"22px",letterSpacing:"2px"}}><i className="fa fa-envelope" aria-hidden="true"></i>&nbsp;care@zenonco.io</span><span><i className="fa fa-search" aria-hidden="true"></i></span>
                        </p>
                    </div>
                </div>
            </div>
            </div>
        )
    }
}
