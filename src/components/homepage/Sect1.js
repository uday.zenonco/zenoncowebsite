import React, { Component } from 'react'

export default class Sect1 extends Component {
    render() {
        return (
            <div>
        
                <nav className="navbar navbar-default" id="grad">
                    <div className="container pr-0">
                        <div className="navbar-header pr-0">
                            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"><span className="icon-bar"></span><span className="icon-bar"></span><span className="icon-bar"></span></button>
                        </div>
                        <div className="col-lg-6 col-md-4 col-sm-3 col-xs-12">
                            &nbsp;
                        </div>
                        <div className="col-lg-6 col-md-8 col-sm-9 col-xs-12">
                        <div className="collapse navbar-collapse pr-0 f-right" id="myNavbar">
                            <ul className="nav navbar-nav">
                                <li><a href="#">Our Services</a></li>
                                <li><a href="#">Free Services</a></li>
                                <li><a href="#">About Cancer</a></li>
                                <li><a href="#">About Us</a></li>
                            </ul>
                        </div>
                        </div>
                    </div>
                </nav>

                <div id="banner-img">
                    <div className="container w-80">
                        <div className="row">
                            <div className="col-md-3 col-md-offset-9 banner-width">
                                <div className="panel panel-default">
                                    <div className="panel-body">
                                        <form className="form-horizontal f-rubik" role="form">
                                            <div><p className="f-bold">Receive detailed guidance on cancer treatment</p></div>
                                            <div className="form-group">
                                                <div className="col-sm-12"><input type="text" className="form-control b-color" id="" placeholder="Name" required="" /></div>
                                            </div>
                                            <div className="form-group">
                                                <div className="col-sm-12"><input type="email" className="form-control b-color" id="" placeholder="Email ID" required="" /></div>
                                            </div>
                                            <div className="form-group">
                                                <div className="col-sm-12"><input type="password" className="form-control b-color" id="inputPassword3" placeholder="Phone No." required="" /></div>
                                            </div>
                                            <div className="form-group">
                                                <div className="col-sm-12 f-13">
                                                    <div className="checkbox">
                                                        <label>
                                                            <input type="checkbox" />
                                                            <p className="f-13">I accept to the Terms &amp; Conditions and <span className="c-color f-bold">Privacy Policy</span> of <span className="c-color f-bold">ZenOnco.io</span></p>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-sm-12"><button className="btn btn-guidance w-100">Get free guidance?</button></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
