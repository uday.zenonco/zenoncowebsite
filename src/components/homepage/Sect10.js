import React, { Component } from 'react';
import testimonials from "../../images/testimonials.png";

export default class Sect10 extends Component {
    render() {
        return (
            <div>
                <div className="pt-20 pb-20" style={{position: "relative",backgroundColor: "rgb(234, 245, 249)"}}>
                    <div className="w-100">
                        <div className="main text-center pb-0">
                            <p className="white f-40 text-grad">TESTIMONIALS</p>
                            <p className="sect2-1 f-rubik f-18 pb-30 color-black">We are happy to have helped thousands of cancer patients in their treatment journey.</p>
                        </div>
                        <div className="container">
                            <div className="col-sm-4">
                                <img src={testimonials} className="img-responsive" />
                            </div>
                            <div class="col-sm-4">
                                <img src={testimonials} className="img-responsive" />
                            </div>
                            <div class="col-sm-4">
                                <img src={testimonials} className="img-responsive" />
                            </div>
                        </div>
                        <br />
                        <button className="btn btn-view1 center-block f-bold">View All</button><br />
                    </div>
                </div>
            </div>
        )
    }
}
