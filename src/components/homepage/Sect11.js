import React, { Component } from 'react';
import ReactDOM from "react-dom";
import Carousel from "react-elastic-carousel";
import Item from "./Item";
import img6 from "../../images/img6.png";


const breakPoints = [
    { width: 1, itemsToShow: 1 },
    { width: 550, itemsToShow: 2 },
    { width: 768, itemsToShow: 3 },
    { width: 1200, itemsToShow: 4 }
];

export default class Sect11 extends Component {
    render() {
        return (
            <div>
                <div class="main text-center pb-0">
                    <p class="white f-40 text-grad">LATEST BLOG</p>
                    <p class="sect2-1 f-rubik f-18 pb-10">Lorem ipsum dolor amet consectetur adipisicing eliteiuim sete eiusmod tempor incididunt</p>
                    <div>&nbsp;</div>
                    <div>&nbsp;</div>

                </div>
                <div className="App pt-20">

                    <Carousel breakPoints={breakPoints} style={{height:"400px"}}>
                        <Item>
                            <div className="text-center">
                                <img src={img6} className="img-responsive" />
                                <p class="f-rubik f-20 pt-20 text-left gray">Can Exercise Halt the Coming and the Progress of Breast Cancer?</p>
                                <p class="f-mont f-14 text-left gray">Heart diseases are the number one cause of mortality in women. Breast cancer is a close...</p>
                                <p class="f-mont f-14 text-left pt-10 text-color">Read More <span class="fa fa-plus"></span></p>
                            </div>
                        </Item>
                        <Item>
                             <div className="text-center">
                                <img src={img6} className="img-responsive" />
                                <p class="f-rubik f-20 pt-20 text-left gray">Can Exercise Halt the Coming and the Progress of Breast Cancer?</p>
                                <p class="f-mont f-14 text-left gray">Heart diseases are the number one cause of mortality in women. Breast cancer is a close...</p>
                                <p class="f-mont f-14 text-left pt-10 text-color">Read More <span class="fa fa-plus"></span></p>
                            </div>
                        </Item>
                        <Item>
                             <div className="text-center">
                                <img src={img6} className="img-responsive" />
                                <p class="f-rubik f-20 pt-20 text-left gray">Can Exercise Halt the Coming and the Progress of Breast Cancer?</p>
                                <p class="f-mont f-14 text-left gray">Heart diseases are the number one cause of mortality in women. Breast cancer is a close...</p>
                                <p class="f-mont f-14 text-left pt-10 text-color">Read More <span class="fa fa-plus"></span></p>
                            </div>
                        </Item>
                        <Item>
                             <div className="text-center">
                                <img src={img6} className="img-responsive" />
                                <p class="f-rubik f-20 pt-20 text-left gray">Can Exercise Halt the Coming and the Progress of Breast Cancer?</p>
                                <p class="f-mont f-14 text-left gray">Heart diseases are the number one cause of mortality in women. Breast cancer is a close...</p>
                                <p class="f-mont f-14 text-left pt-10 text-color">Read More <span class="fa fa-plus"></span></p>
                            </div>
                        </Item>
                        <Item>
                             <div className="text-center">
                                <img src={img6} className="img-responsive" />
                                <p class="f-rubik f-20 pt-20 text-left gray">Can Exercise Halt the Coming and the Progress of Breast Cancer?</p>
                                <p class="f-mont f-14 text-left gray">Heart diseases are the number one cause of mortality in women. Breast cancer is a close...</p>
                                <p class="f-mont f-14 text-left pt-10 text-color">Read More <span class="fa fa-plus"></span></p>
                            </div>
                        </Item>
                        <Item>
                             <div className="text-center">
                                <img src={img6} className="img-responsive" />
                                <p class="f-rubik f-20 pt-20 text-left gray">Can Exercise Halt the Coming and the Progress of Breast Cancer?</p>
                                <p class="f-mont f-14 text-left gray">Heart diseases are the number one cause of mortality in women. Breast cancer is a close...</p>
                                <p class="f-mont f-14 text-left pt-10 text-color">Read More <span class="fa fa-plus"></span></p>
                            </div>
                        </Item>
                        <Item>
                             <div className="text-center">
                                <img src={img6} className="img-responsive" />
                                <p class="f-rubik f-20 pt-20 text-left gray">Can Exercise Halt the Coming and the Progress of Breast Cancer?</p>
                                <p class="f-mont f-14 text-left gray">Heart diseases are the number one cause of mortality in women. Breast cancer is a close...</p>
                                <p class="f-mont f-14 text-left pt-10 text-color">Read More <span class="fa fa-plus"></span></p>
                            </div>
                        </Item>
                        <Item>
                             <div className="text-center">
                                <img src={img6} className="img-responsive" />
                                <p class="f-rubik f-20 pt-20 text-left gray">Can Exercise Halt the Coming and the Progress of Breast Cancer?</p>
                                <p class="f-mont f-14 text-left gray">Heart diseases are the number one cause of mortality in women. Breast cancer is a close...</p>
                                <p class="f-mont f-14 text-left pt-10 text-color">Read More <span class="fa fa-plus"></span></p>
                            </div>
                        </Item>
                    </Carousel>
                </div>
                <div className="center-block" style={{paddingBottom:"20px"}}><button className="btn btn-default center-block view-btn f-bold">View All</button></div>
            </div>
        )
    }
}
