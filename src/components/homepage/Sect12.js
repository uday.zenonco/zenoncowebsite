import React, { Component } from 'react';
import img7 from "../../images/img7.png";

export default class Sect12 extends Component {
    render() {
        return (
            <div>
                <div className="bg-color" style={{position:"relative"}}>
                    <div className="w-100">
                        <div className="main text-center pb-0"><p className="white f-40 text-grad">READ MORE ABOUT CANCER</p></div>
                        <div className="container">
                            <div className="col-sm-4">
                                <img src={img7} className="image-responsive center-block pb-10"
                                    style={{ width: "200px" }} />
                                <p className="text-center"><span className="t-left">TYPE </span><span className="t-right">OF CANCER</span></p>
                                <select className="form-control f-dd mb-30">
                                    <option>Select the article for read more</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                </select>
                            </div>
                            <div className="col-sm-4">
                                <img src={img7} className="image-responsive center-block pb-10"
                                    style={{ width: "200px" }} />
                                <p className="text-center"><span className="t-left">SIDE EFFECTS </span><span className="t-right">OF CANCER</span></p>
                                <select className="form-control f-dd mb-30">
                                    <option>Select the article for read more</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                </select>
                            </div>
                            <div className="col-sm-4">
                                <img src={img7} className="image-responsive center-block pb-10"
                                    style={{ width: "200px" }} />
                                <p className="text-center"><span className="t-left">TREATMENT</span><span className="t-right">APPROACH</span></p>
                                <select className="form-control f-dd mb-30">
                                    <option>Select the article for read more</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
