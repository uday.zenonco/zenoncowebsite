import React, { Component } from 'react'

export default class Sect13 extends Component {
    render() {
        return (
            <div>
                <div id="grad">
                    <div style={{position:"relative"}}>
                        <div className="w-100">
                            <div className="main text-center pb-0">
                                <p className="white f-40 sign-up-text">SIGN UP FOR CANCER RESOURCES</p>
                                <div className="input-group pb-20" style={{width:"50%",margin:"0 auto",paddingTop:"20px",paddingBottom:"20px"}}>
                                    <div className="inner-addon right-addon"><i className="glyphicon glyphicon-search" style={{zIndex:"9999"}}></i><input type="text" className="form-control" placeholder="Enter your email address" /></div>
                                    <br />
                                    <br />
                                    <br />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
