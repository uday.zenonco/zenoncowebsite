import React, { Component } from 'react';
import logo from "../../images/logo.png";


export default class Sect14 extends Component {
    render() {
        return (
            <div>
                <div style={{ position: "relative" }}>
                    <div style={{ backgroundColor: "rgb(33, 33, 33)", color: "rgb(255, 255, 255)", width: "100%" }}>
                        <div class="container" style={{ padding: "40px" }}>
                            <div class="col-sm-3">
                                <p class="f-rubik f-16 f-bold">SERVICES</p>
                                <ul class="f-mont f-13 ul-style" style={{ listStyleType: "none", padding: "0px" }}>
                                    <li>Get free integrative oncology report</li>
                                    <li>Get guidance with an onco expert</li>
                                    <li>Book consultation with oncologist</li>
                                    <li>Book diagnostic test</li>
                                </ul>
                            </div>
                            <div class="col-sm-3">
                                <p class="f-rubik f-16 f-bold">CANCER RESOURCES</p>
                                <ul class="f-mont f-13 ul-style" style={{ listStyleType: "none", padding: "0px" }}>
                                    <li>Types of cancer</li>
                                    <li>Side effects and symptoms</li>
                                    <li>Articles &amp; blogs</li>
                                    <li>Cancer healing stories</li>
                                    <li>Documents</li>
                                    <li>Podcasts</li>
                                    <li>Stories</li>
                                </ul>
                            </div>
                            <div class="col-sm-3">
                                <p class="f-rubik f-16 f-bold">OTHER LINKS</p>
                                <ul class="f-mont f-13 ul-style" style={{ listStyleType: "none", padding: "0px" }}>
                                    <li>Join our community</li>
                                    <li>FAQs</li>
                                    <li>Need and Media</li>
                                    <li>CareTeam</li>
                                    <li>Calender</li>
                                    <li>Gallery</li>
                                </ul>
                            </div>
                            <div class="col-sm-3">
                                <p class="f-rubik f-16 f-bold">CONTACT US</p>
                                <ul class="f-mont f-13 ul-style" style={{ listStyleType: "none", padding: "0px" }}>
                                    <li><span class="fa fa-phone pr-10"></span>+91 937 297 6783</li>
                                    <li><span class="fa fa-envelope pr-10"></span> care@ZenOnco.io</li>
                                    <li>
                                        <span class="fa fa-map-marker pr-10"></span> ZenOnco Healing Center, 136, <br />
                                    10th Road, Near Zahra Court, Khar West, <br />
                                    Mumbai - 400 052, Maharashtra India
                                </li>
                                </ul>
                                <div class="demopadding" style={{display:"flex"}}>
                                    <div class="icon social fb"><i class="fa fa-facebook"></i></div>
                                    <div class="icon social fb tw"><i class="fa fa-twitter"></i></div>
                                    <div class="icon social fb in"><i class="fa fa-linkedin"></i></div>
                                    <div class="icon social fb in"><i class="fa fa-instagram"></i></div>
                                    <div class="icon social fb in"><i class="fa fa-youtube"></i></div>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="container">
                            <div class="col-sm-4">
                               <img src={logo} alt="" style={{backgroundColor:"transparent!important"}} style={{paddingBottom:"20px"}} />
                            </div>
                            <div class="col-sm-4"><p class="f-mont f-13 pt-22">Terms &amp; Conditions | Privacy Policy</p></div>
                            <div class="col-sm-4"><p class="f-mont f-12 pt-22">© Copyright 2020 ZenOnco. All rights reserved.</p></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
