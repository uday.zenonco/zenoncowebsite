import React, { Component } from 'react'

export default class Sect15 extends Component {
    render() {
        return (
            <div>
                <div style={{position:"relative"}}>
                <div className="w-100">
                    <div className="container center-block" style={{margin:"0 auto",left:"0px",right:"0px"}}>
                        <div className="col-md-4 text-center pt-20 pb-20" style={{width:"100%"}}>
                            <button
                                type="button"
                                className="btn btn-lg text-center f-rubik f-16 bg-white"
                                style={{backgroundColor:"rgb(255, 255, 255)",border:"1px solid rgb(46, 137, 171)",color:"rgb(46, 137, 171)",marginRight:"15px"}}
                            >
                                <span className="fa fa-phone"></span>&nbsp;Call Us!
                            </button>
                            <button
                                type="button"
                                className="btn btn-lg text-center f-rubik f-16 bg-white"
                                style={{backgroundColor:"rgb(255, 255, 255)",border:"1px solid rgb(46, 137, 171)",color:"rgb(46, 137, 171)",marginRight:"15px"}}
                            >
                                <span className="fa fa-whatsapp"></span>&nbsp;Whatsapp
                            </button>
                            <button
                                type="button"
                                className="btn btn-lg text-center f-rubik f-16 bg-white"
                                style={{backgroundColor:"rgb(255, 255, 255)",border:"1px solid rgb(46, 137, 171)",color:"rgb(46, 137, 171)",marginRight:"15px"}}
                            >
                                <i className="fa fa-comment" aria-hidden="true"></i>&nbsp;Leave a Message
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        )
    }
}
