import React, { Component } from 'react'
import img1 from "../../images/img1.png";


export default class Sect2 extends Component {
    render() {
        return (
            <div>
                 <div id="grad-sect1">
            <div className="main text-center container">
                <div className="row">
                    <div className="mb-45 mt-20">
                        <span className="f-40 f-rubik f-bold">CANCER CAN BE CURED</span><br />
                        <span className="f-24 f-rubik">With The Right Approach</span>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-2">
                       <img src={img1}
                            className="image-responsive mb-20 w-100 "
                        />
                        <p className="sect1-1 f-rubik">MEET THE RIGHT ONCOLOGIST</p>
                        <p className="f-mont f-13">Suggestions depending on your cancer profile</p>
                    </div>
                    <div className="col-sm-2">
                       <img src={img1}
                            className="image-responsive mb-20 w-100"
                        />
                        <p className="sect1-1 f-rubik">COMPLETE DIAGNOSTIC TESTING</p>
                        <p className="f-mont f-13">To identify best treatment possible</p>
                    </div>
                    <div className="col-sm-2">
                       <img src={img1}
                            className="image-responsive mb-20 w-100"
                        />
                        <p className="sect1-1 f-rubik">COMPLEMENTARY TREATMENT</p>
                        <p className="f-mont f-13">Consult with an onco nutritionist, onco-psychologist and fitness trainer</p>
                    </div>
                    <div className="col-sm-2">
                       <img src={img1}
                            className="image-responsive mb-20 w-100"
                        />
                        <p className="sect1-1 f-rubik">CONNECT WITH COMMUNITY</p>
                        <p className="f-mont f-13">Share experiences with cancer survivors,patients and their caregivers</p>
                    </div>
                    <div className="col-sm-2">
                       <img src={img1}
                            className="image-responsive mb-20 w-100"
                        />
                        <p className="sect1-1 f-rubik">CANCER HEALING JOURNEYS</p>
                        <p className="f-mont f-13">Listen to cancer warriors as they share their experiences</p>
                    </div>
                    <div className="col-sm-2">
                       <img src={img1}
                            className="image-responsive mb-20 w-100"
                        />
                        <p className="sect1-1 f-rubik">CANCER CARE AT HOME</p>
                        <p className="f-mont f-13">Get chemotherapy, palliative care and nusrsing at home</p>
                    </div>
                </div>
            </div>
        </div>
            </div>
        )
    }
}
