import React, { Component } from 'react';
import ReactDOM from "react-dom";
import Carousel from "react-elastic-carousel";
import Item from "./Item";
import doc1 from "../../images/doc1.png";


const breakPoints = [
    { width: 1, itemsToShow: 1 },
    { width: 550, itemsToShow: 2 },
    { width: 768, itemsToShow: 3 },
    { width: 1200, itemsToShow: 4 }
];

export default class Sect3 extends Component {
    render() {
        return (
            <div>
                <div class="main text-center pb-0">
                    <p class="white f-40 text-grad">MEET THE RIGHT ONCOLOGIST</p>
                    <p class="sect2-1 f-rubik f-18 pb-10">Based on cancer type and medical condition | 1,500+ oncologists across India | Video counseling also available in select cases</p>
                    <div>&nbsp;</div>
                    <div>&nbsp;</div>

                </div>
                <div className="App pt-20">

                    <Carousel breakPoints={breakPoints} style={{ height: "410px" }}>
                        <Item>
                            <div className="text-center">
                                <img src={doc1} className="img-responsive" />
                                <p className="sect2-2">Dr.Ken Morgan</p>
                                <p className="sect2-3">Medical Oncologist</p>
                                <p className="sect2-4">9 Yrs exp | Mumbai</p>
                                <p className="mb-10"><button className="btn sect2-btn btn-default mb-30">Profile</button>&nbsp;&nbsp;<button class="btn sect2-btn btn-default mb-30">Consult</button></p>
                            </div>
                        </Item>
                        <Item>
                            <div className="text-center">
                                <img src={doc1} className="img-responsive" />
                                <p className="sect2-2">Dr.Ken Morgan</p>
                                <p className="sect2-3">Medical Oncologist</p>
                                <p className="sect2-4">9 Yrs exp | Mumbai</p>
                                <p className="mb-10"><button className="btn sect2-btn btn-default mb-30">Profile</button>&nbsp;&nbsp;<button class="btn sect2-btn btn-default mb-30">Consult</button></p>
                            </div>
                        </Item>
                        <Item>
                            <div className="text-center">
                                <img src={doc1} className="img-responsive" />
                                <p className="sect2-2">Dr.Ken Morgan</p>
                                <p className="sect2-3">Medical Oncologist</p>
                                <p className="sect2-4">9 Yrs exp | Mumbai</p>
                                <p className="mb-10"><button className="btn sect2-btn btn-default mb-30">Profile</button>&nbsp;&nbsp;<button class="btn sect2-btn btn-default mb-30">Consult</button></p>
                            </div>
                        </Item>
                        <Item>
                            <div className="text-center">
                                <img src={doc1} className="img-responsive" />
                                <p className="sect2-2">Dr.Ken Morgan</p>
                                <p className="sect2-3">Medical Oncologist</p>
                                <p className="sect2-4">9 Yrs exp | Mumbai</p>
                                <p className="mb-10"><button className="btn sect2-btn btn-default mb-30">Profile</button>&nbsp;&nbsp;<button class="btn sect2-btn btn-default mb-30">Consult</button></p>
                            </div>
                        </Item>
                        <Item>
                            <div className="text-center">
                                <img src={doc1} className="img-responsive" />
                                <p className="sect2-2">Dr.Ken Morgan</p>
                                <p className="sect2-3">Medical Oncologist</p>
                                <p className="sect2-4">9 Yrs exp | Mumbai</p>
                                <p className="mb-10"><button className="btn sect2-btn btn-default mb-30">Profile</button>&nbsp;&nbsp;<button class="btn sect2-btn btn-default mb-30">Consult</button></p>
                            </div>
                        </Item>
                        <Item>
                            <div className="text-center">
                                <img src={doc1} className="img-responsive" />
                                <p className="sect2-2">Dr.Ken Morgan</p>
                                <p className="sect2-3">Medical Oncologist</p>
                                <p className="sect2-4">9 Yrs exp | Mumbai</p>
                                <p className="mb-10"><button className="btn sect2-btn btn-default mb-30">Profile</button>&nbsp;&nbsp;<button class="btn sect2-btn btn-default mb-30">Consult</button></p>
                            </div>
                        </Item>
                        <Item>
                            <div className="text-center">
                                <img src={doc1} className="img-responsive" />
                                <p className="sect2-2">Dr.Ken Morgan</p>
                                <p className="sect2-3">Medical Oncologist</p>
                                <p className="sect2-4">9 Yrs exp | Mumbai</p>
                                <p className="mb-10"><button className="btn sect2-btn btn-default mb-30">Profile</button>&nbsp;&nbsp;<button class="btn sect2-btn btn-default mb-30">Consult</button></p>
                            </div>
                        </Item>
                        <Item>
                            <div className="text-center">
                                <img src={doc1} className="img-responsive" />
                                <p className="sect2-2">Dr.Ken Morgan</p>
                                <p className="sect2-3">Medical Oncologist</p>
                                <p className="sect2-4">9 Yrs exp | Mumbai</p>
                                <p className="mb-10"><button className="btn sect2-btn btn-default mb-30">Profile</button>&nbsp;&nbsp;<button class="btn sect2-btn btn-default mb-30">Consult</button></p>
                            </div>
                        </Item>
                    </Carousel>
                </div>
                <br />
                <div className="center-block"><button className="btn btn-default center-block view-btn f-bold">View All</button></div>
                <hr />
            </div>
        )
    }
}
