import React, { Component } from 'react';
import doc1 from "../../images/doc1.png";
import img3 from "../../images/img3.png";



export default class Sect4 extends Component {
    render() {
        return (
            <div>
                <div className="main text-center pb-50">
                    <p className="text-grad f-40">COMPLETE DIAGNOSTIC TESTING</p>
                    <p className="sect2-1">Free counseling on which types of test to do. | Home pick-up of samples across 200+ cities | Avail upto 10% of discount</p>
                </div>

                <div id="diagnostic-testing">
                    <div className="container" style={{ border: "" }}>
                        <div className="dt1 w-100" style={{ width: "33%", float: "left", border: "", padding: "10px" }}>
                            <a href="#1">
                                <figure>
                                    <img src={img3} alt="" style={{ width: "97%", height: "425px" }} />
                                </figure>
                                <p id="grad2">BLOOD TEST</p>
                                <p id="grad3">
                                    <span><span className="fa fa-plus-circle p-5"></span>&nbsp;Complete Blood Count</span> <br />
                                    <span><span className="fa fa-plus-circle p-5"></span>&nbsp;Chemistry panel (metabolic profile)</span> <br />
                                    <span><span className="fa fa-plus-circle p-5"></span>&nbsp;Tumor marker testing: CA-125, estrogen, progesterone, CEA, EGFR, PCA3 mRNA and PSA</span> <br />
                                    <span><span className="fa fa-plus-circle p-5"></span>&nbsp;Genetic mutation testing: BRCA1, BRCA2, TP53, MSI status, BRAF, KRAS, NRAS</span> <br />
                                    <span><span className="fa fa-plus-circle p-5"></span>&nbsp;Cytogenetic analysis (chromosomes)</span>
                                </p>
                            </a>
                        </div>
                        <div className="dt1 w-100" style={{ width: "33%", float: "left", border: "", padding: "10px" }}>
                            <a href="#1">
                                <figure>
                                    <img src={img3} alt="" style={{ width: "97%",height:"425px" }} />
                                </figure>
                                <p id="grad2">IMAGING TEST</p>
                                <p id="grad3">
                                    <span><span className="fa fa-plus-circle p-5"></span>&nbsp;CT scan</span> <br />
                                    <span><span className="fa fa-plus-circle p-5"></span>&nbsp;MRI scan</span> <br />
                                    <span><span className="fa fa-plus-circle p-5"></span>&nbsp;Radiographic tests (X-rays, mammogram, angiography, IVP, barium enema, upper GI series, venography) and PSA</span> <br />
                                    <span><span className="fa fa-plus-circle p-5"></span>&nbsp;Nuclear medicine scans (bone scans, PET scans, thyroid scans, MUGA, gallium scans)</span> <br />
                                    <span><span className="fa fa-plus-circle p-5"></span>&nbsp;Ultrasound</span>
                                </p>

                            </a>
                        </div>
                        <div className="dt1 w-100" style={{ width: "33%", float: "left", border: "", padding: "10px" }}>
                            <a href="#1">
                                <figure>
                                    <img src={img3} alt="" style={{ width: "97%",height:"425px" }} />
                                </figure>
                                <p id="grad2">OTHER TEST</p>
                                <p id="grad3">
                                    <span><span className="fa fa-plus-circle p-5"></span>&nbsp;Sputum cytology</span> <br />
                                    <span><span className="fa fa-plus-circle p-5"></span>&nbsp;Urinalysis</span> <br />
                                    <span><span className="fa fa-plus-circle p-5"></span>&nbsp;Electrocardiogram and echodiagram and PSA</span> <br />
                                    <span><span className="fa fa-plus-circle p-5"></span>&nbsp;Fecal occult blood test</span> <br />
                                    <span><span className="fa fa-plus-circle p-5"></span>&nbsp;Cerebrospinal fluid test</span>
                                </p>
                            </a>
                        </div>
                    </div>
                    <div style={{position:"absolute",border:"",width:"100%"}}>
                    <button className="btn center-block mt-20 mb-20 f-rubik f-18 f-btn">Book a diagnostic test</button>
                    </div>
                </div>

                
            </div>
        )
    }
}
