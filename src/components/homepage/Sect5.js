import React, { Component } from 'react';
import img8 from "../../images/img8.png";


export default class Sect5 extends Component {
    render() {
        return (
            <div>
                <div class="main text-center pt-100" style={{paddingTop:"100px",paddingRight:"25px"}}>
                    <p class="text-grad f-40 font-30">COMPLEMENTARY AND ALTERNATIVE MEDICINE</p>
                    <p class="sect2-1">Complete your medical treatment by combining it with complementary treatment to increase its clinical efficacy while reducing its side effects</p>
                </div>

                <div id="diagnostic-testing1">
                    <div className="container" style={{ border: "" }}>
                        <div className="dt1 w-100" style={{ width: "33%", float: "left", border: "", padding: "10px" }}>
                            <a href="#1">
                                <figure>
                                    <img src={img8} alt="" style={{ width: "97%", height: "425px" }} />
                                </figure>
                                <p id="grad4">BLOOD TEST</p>
                                <p id="grad5" className="pt-10">
                                    <span><span className="fa fa-check-circle color-gray"></span>&nbsp;Anti-cancer food</span> <br />
                                    <span><span className="fa fa-check-circle color-gray"></span>&nbsp;Meal-wise diet plan</span> <br />
                                    <span><span className="fa fa-check-circle color-gray"></span>&nbsp;Supplements and nutraceuticals</span> <br />
                                </p>
                            </a>
                        </div>
                        <div className="dt1 w-100" style={{ width: "33%", float: "left", border: "", padding: "10px" }}>
                            <a href="#1">
                                <figure>
                                    <img src={img8} alt="" style={{ width: "97%",height:"425px" }} />
                                </figure>
                                <p id="grad4">FITNESS COUNSELING</p>
                                <p id="grad5" className="pt-10">
                                    <span><span className="fa fa-check-circle color-gray"></span>&nbsp;Strengthen your body</span> <br />
                                    <span><span className="fa fa-check-circle color-gray"></span>&nbsp;Increase immunity</span> <br />
                                    <span><span className="fa fa-check-circle color-gray"></span>&nbsp;Minimize side effects of treatment</span> <br />
                                </p>

                            </a>
                        </div>
                        <div className="dt1 w-100" style={{ width: "33%", float: "left", border: "", padding: "10px" }}>
                            <a href="#1">
                                <figure>
                                    <img src={img8} alt="" style={{ width: "97%",height:"425px" }} />
                                </figure>
                                <p id="grad4">EMOTIONAL COUNSELING</p>
                                <p id="grad5" className="pt-10">
                                    <span><span className="fa fa-check-circle color-gray"></span>&nbsp;Manage stress</span> <br />
                                    <span><span className="fa fa-check-circle color-gray"></span>&nbsp;Mindfulness exercises</span> <br />
                                    <span><span className="fa fa-check-circle color-gray"></span>&nbsp;Mind-body medicine</span> <br />
                                </p>
                            </a>
                        </div>
                    </div>
                    <div style={{position:"absolute",border:"",width:"100%"}}>
                    <button className="btn center-block mt-20 mb-20 f-rubik f-18 f-btn btn-color color-white">Consult</button>
                    </div>
                </div>

            </div>
        )
    }
}
