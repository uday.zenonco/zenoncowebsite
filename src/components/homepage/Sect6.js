import React, { Component } from 'react'

export default class Sect6 extends Component {
    render() {
        return (
            <div>
                 <div id="grad1" className="pt-50 pb-50">
                <div className="container white">
                    <p className="f-40 white f-rubik text-center">ZENONCO.IO</p>
                    <p className="f-18 white f-rubik text-center pb-30">Integrative Oncology Preliminary Assessment Report (ZIOPAR)</p>
                    <div className="col-sm-4">
                        <div className="form-group">
                            <label for="">CANCER TYPE</label>
                            <select className="form-white" id="form">
                                <option>Select</option>
                                <option id="transparent">2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group">
                            <label for="">CANCER STAGE</label>
                            <select className="form-white" id="">
                                <option>Select</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group">
                            <label for="">PERFORMANCE SCORE</label>
                            <select className="form-white" id="">
                                <option>Select</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group">
                            <label for="">PRIOR TREATMENT COMPLETED</label>
                            <select className="form-white" id="">
                                <option>Select</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group">
                            <label for="">OTHER DISEASES</label>
                            <select className="form-white" id="" style={{backgroundColor:"transparent"}}>
                                <option style={{backgroundColor:"transparent"}}>Select</option>
                                <option style={{backgroundColor:"transparent"}}>2</option>
                                <option style={{backgroundColor:"transparent"}}>3</option>
                                <option style={{backgroundColor:"transparent"}}>4</option>
                            </select>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group">
                            <label for="">SYMPTOMS AND SIDE-EFFECTS</label>
                            <select className="form-white" id="">
                                <option>Select</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </div>
                    <div className="col-sm-4"><label for="">NAME</label><input type="text" className="form-white name1" placeholder="Enter Name" /></div>
                    <div className="col-sm-4"><label for="">EMAIL ID</label><input type="text" className="form-white name1" placeholder="Enter Email to receive report" /></div>
                    <div className="col-sm-4"><label for="">PHONE NO.</label><input type="text" className="form-white name1" placeholder="Enter phone No." /></div>
                    <div className="col-sm-4">
                        <label for="">&nbsp;</label>
                        <p className="f-rubik">CAPTCHA</p>
                    </div>
                    <div className="col-sm-4">
                        <label for="">&nbsp;</label>
                        <p className="f-rubik">
                            <label className="checkbox-inline"><input type="checkbox" value="" />I accept to Terms &amp; Conditions and Privacy Policy of ZenOnco.io</label>
                        </p>
                    </div>
                    <div className="col-sm-4">
                        <label for="">&nbsp;</label>
                        <p className="f-rubik"><button className="btn btn-free-report f-rubik f-bold">Receive free report</button></p>
                    </div>
                </div>
            </div>
            </div>
        )
    }
}
