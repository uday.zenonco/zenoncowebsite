import React, { Component } from 'react'
import img4 from "../../images/img4.png";

export default class Sect7 extends Component {
    render() {
        return (
            <div>
                <div style={{position:"relative"}}>
                <div className="w-100">
                    <div class="main text-center" style={{paddingBottom:"40px"}}>
                        <p class="text-grad f-40">CONNECT WITH COMMUNITY</p>
                        <p class="sect2-1 f-rubik f-16">Lorem ipsum dolor amet consectetur adipisicing eliteiuim sete eiusmod tempor incididunt ut labore etnalom dolore magna aliqua udiminimate veniam quis norud.</p>
                    </div>
                    <div class="container">
                        <div class="row ten-columns">
                            <div class="col-sm-2 cl"><img src={img4} class="img-responsive width-100"  /></div>
                            <div class="col-sm-2 cl"><img src={img4} class="img-responsive width-100"  /></div>
                            <div class="col-sm-2 cl"><img src={img4} class="img-responsive width-100"  /></div>
                            <div class="col-sm-2 cl"><img src={img4} class="img-responsive width-100"  /></div>
                            <div class="col-sm-2 cl"><img src={img4} class="img-responsive width-100"  /></div>
                        </div>
                        <div class="row ten-columns">
                            <div class="col-sm-2 cl"><img src={img4} class="img-responsive width-100"  /></div>
                            <div class="col-sm-2 cl"><img src={img4} class="img-responsive width-100"  /></div>
                            <div class="col-sm-2 cl"><img src={img4} class="img-responsive width-100"  /></div>
                            <div class="col-sm-2 cl"><img src={img4} class="img-responsive width-100"  /></div>
                            <div class="col-sm-2 cl"><img src={img4} class="img-responsive width-100"  /></div>
                        </div>
                        <div class="row ten-columns">
                            <div class="col-sm-2 cl"><img src={img4} class="img-responsive width-100"  /></div>
                            <div class="col-sm-2 cl"><img src={img4} class="img-responsive width-100"  /></div>
                            <div class="col-sm-2 cl"><img src={img4} class="img-responsive width-100"  /></div>
                            <div class="col-sm-2 cl"><img src={img4} class="img-responsive width-100"  /></div>
                            <div class="col-sm-2 cl"><img src={img4} class="img-responsive width-100"  /></div>
                        </div>
                        <div class="col-sm-12 mb-30 mt-30"><button class="btn btn-consult-style center-block">Connect with the community</button></div>
                    </div>
                    <div></div>
                </div>
            </div>
            </div>
        )
    }
}
