import React, { Component } from 'react';
import join_us from "../../images/join_us.png";


export default class Sect8 extends Component {
    render() {
        return (
            <div>
                <div id="grad1" class="pb-30">
                    <div style={{position:"relative"}}>
                        <div className="w-100">
                            <div class="main text-center" style={{paddingBottom:"40px"}}>
                                <p class="white f-40">JOIN US IN COMMUNITY EVENTS</p>
                                <p class="sect2-1 f-rubik f-20 white">Join us in our upcoming events, and listen to speakers live while they share their experiences with cancer and tips to cure cancer.</p>
                            </div>
                            <div class="container">
                                <div class="col-sm-4">
                                    <img src={join_us} className="img-responsive" />
                                    <p class="f-14 white f-rubik pt-20">
                                        HEALING CIRCLE TALKS<br />
                                        <span class="f-24 white f-rubik">with Dr. Darshana Thakkar</span>
                                    </p>
                                    <p class="f-mont f-14 white">An intl certified healer, emotional wellness coach, motivational speaker and a Super-Doctor.</p>
                                    <button class="btn btn-register">Register</button>
                                </div>
                                <div class="col-sm-4">
                                    <img src={join_us} className="img-responsive" />
                                    <p class="f-14 white f-rubik pt-20">
                                        HEALING CIRCLE TALKS<br />
                                        <span class="f-24 white f-rubik">with Dr. Darshana Thakkar</span>
                                    </p>
                                    <p class="f-mont f-14 white">An intl certified healer, emotional wellness coach, motivational speaker and a Super-Doctor.</p>
                                    <button class="btn btn-register">Register</button>
                                </div>
                                <div class="col-sm-4">
                                    <img src={join_us} className="img-responsive" />
                                    <p class="f-14 white f-rubik pt-20">
                                        HEALING CIRCLE TALKS<br />
                                        <span class="f-24 white f-rubik">with Dr. Darshana Thakkar</span>
                                    </p>
                                    <p class="f-mont f-14 white">An intl certified healer, emotional wellness coach, motivational speaker and a Super-Doctor.</p>
                                    <button class="btn btn-register">Register</button>
                                </div>
                            </div>
                            <button class="btn center-block mt-20 mb-20 btn-view">View All</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
