import React, { Component } from 'react';
import ReactDOM from "react-dom";
import Carousel from "react-elastic-carousel";
import Item from "./Item";
import img5 from "../../images/img5.png";


const breakPoints = [
    { width: 1, itemsToShow: 1 },
    { width: 550, itemsToShow: 2 },
    { width: 768, itemsToShow: 3 },
    { width: 1200, itemsToShow: 4 }
];

export default class Sect9 extends Component {
    render() {
        return (
            <div>
                <div class="main text-center pb-0">
                    <p class="white f-40 text-grad">MANY PEOPLE HAVE DEFEATED CANCER</p>
                    <p class="sect2-1 f-rubik f-18 pb-10">Lorem ipsum dolor amet consectetur adipisicing eliteiuim sete eiusmod tempor incididunt</p>
                    <div>&nbsp;</div>
                    <div>&nbsp;</div>

                </div>
                <div className="App pt-20">

                    <Carousel breakPoints={breakPoints}>
                        <Item>
                            <div className="text-center">
                                <img src={img5} className="img-responsive" />
                                <p class="f-rubik f-20 text-left" style={{color:"#333"}}>Sandeep Patil</p>
                                <p class="f-mont f-13 text-left" style={{color:"#333"}}>Lorem ipsum dolor sit amet, consectetur ad piscing elit, sed do eiusmod tempor incidiut labore dolore piscing elit.<br/><span className="text-color">Read More <span class="fa fa-plus"></span></span></p>
                                {/* <p class="f-mont f-14 text-left pt-10 text-color"></p> */}
                            </div>
                        </Item>
                        <Item>
                            <div className="text-center">
                                <img src={img5} className="img-responsive" />
                                 <p class="f-rubik f-20 text-left" style={{color:"#333"}}>Sandeep Patil</p>
                                <p class="f-mont f-13 text-left" style={{color:"#333"}}>Lorem ipsum dolor sit amet, consectetur ad piscing elit, sed do eiusmod tempor incidiut labore dolore piscing elit.<br/><span className="text-color">Read More <span class="fa fa-plus"></span></span></p>
                            </div>
                        </Item>
                        <Item>
                            <div className="text-center">
                                <img src={img5} className="img-responsive" />
                                 <p class="f-rubik f-20 text-left" style={{color:"#333"}}>Sandeep Patil</p>
                                <p class="f-mont f-13 text-left" style={{color:"#333"}}>Lorem ipsum dolor sit amet, consectetur ad piscing elit, sed do eiusmod tempor incidiut labore dolore piscing elit.<br/><span className="text-color">Read More <span class="fa fa-plus"></span></span></p>
                            </div>
                        </Item>
                        <Item>
                            <div className="text-center">
                                <img src={img5} className="img-responsive" />
                                 <p class="f-rubik f-20 text-left" style={{color:"#333"}}>Sandeep Patil</p>
                                <p class="f-mont f-13 text-left" style={{color:"#333"}}>Lorem ipsum dolor sit amet, consectetur ad piscing elit, sed do eiusmod tempor incidiut labore dolore piscing elit.<br/><span className="text-color">Read More <span class="fa fa-plus"></span></span></p>
                            </div>
                        </Item>
                        <Item>
                            <div className="text-center">
                                <img src={img5} className="img-responsive" />
                                 <p class="f-rubik f-20 text-left" style={{color:"#333"}}>Sandeep Patil</p>
                                <p class="f-mont f-13 text-left" style={{color:"#333"}}>Lorem ipsum dolor sit amet, consectetur ad piscing elit, sed do eiusmod tempor incidiut labore dolore piscing elit.<br/><span className="text-color">Read More <span class="fa fa-plus"></span></span></p>
                            </div>
                        </Item>
                        <Item>
                            <div className="text-center">
                                <img src={img5} className="img-responsive" />
                                 <p class="f-rubik f-20 text-left" style={{color:"#333"}}>Sandeep Patil</p>
                                <p class="f-mont f-13 text-left" style={{color:"#333"}}>Lorem ipsum dolor sit amet, consectetur ad piscing elit, sed do eiusmod tempor incidiut labore dolore piscing elit.<br/><span className="text-color">Read More <span class="fa fa-plus"></span></span></p>
                            </div>
                        </Item>
                        <Item>
                            <div className="text-center">
                                <img src={img5} className="img-responsive" />
                                 <p class="f-rubik f-20 text-left" style={{color:"#333"}}>Sandeep Patil</p>
                                <p class="f-mont f-13 text-left" style={{color:"#333"}}>Lorem ipsum dolor sit amet, consectetur ad piscing elit, sed do eiusmod tempor incidiut labore dolore piscing elit.<br/><span className="text-color">Read More <span class="fa fa-plus"></span></span></p>
                            </div>
                        </Item>
                        <Item>
                            <div className="text-center">
                                <img src={img5} className="img-responsive" />
                                 <p class="f-rubik f-20 text-left" style={{color:"#333"}}>Sandeep Patil</p>
                                <p class="f-mont f-13 text-left" style={{color:"#333"}}>Lorem ipsum dolor sit amet, consectetur ad piscing elit, sed do eiusmod tempor incidiut labore dolore piscing elit.<br/><span className="text-color">Read More <span class="fa fa-plus"></span></span></p>
                            </div>
                        </Item>
                    </Carousel>
                </div>
                <div className="center-block" style={{paddingBottom:"20px"}}><button className="btn btn-default center-block view-btn f-bold">View All</button></div>
            </div>
        )
    }
}
